# Data Valuation Tool from Market Perspective (DVTMP)

Data valuation tool provides an estimation of the audiences monetary value for advertising in online social media platforms (Facebook, instagram and linkedin)

# Introduction

The Data Valuation Tools from the market perspective (DVTMP) module developed in PIMCity is leveraging some of the most popular existing online advertising platforms to estimate the value of hundreds to thousands of audiences. The DVTMP module aims to provide the monetary value of audiences traded on the main online advertising platforms. This serves any PIM deciding to implement the DVTMP module to have a realistic estimation of audiences' value to be traded. Since the information about values collected from these advertising platforms is based on aggregated historical pricing data, we can assert that DVTMP provides full-privacy guarantees. Moreover, a given audience's value can be obtained in real-time from the referred online advertising platform. 

<img src="./DVTMP.png" alt="flowchart" width="500"/>

## Technical Design

The DVTMP module's goal consists of developing a software module that extracts audiences' prices from different popular advertising platforms, stores them, and delivers them to the PimCity Trading Engine. To obtain a realistic market price of audiences, the DVTMP queries the Marketing API of popular advertising platforms (Facebook, Instagram, and LinkedIn) to obtain the reported price advertisers pay for delivering ads to specific audiences. This module queries the marketing APIs specifying the parameters of the audience being targeted and obtains in return an estimated price of the audience in the form of standard price metrics such as the price for showing 1000 ads to users from that audience (CPM) or the price of a click (CPC). The market value for each queried audience is locally stored in its cache memory.  
The flow chart shown below summarizes the conceptual execution process of the DVTMP module for a specific advertising platform. Note that there is an independent submodule taking care of each advertising platform. This design provides flexibility to easily extend the DVTMP to include new data sources from other advertising platforms. The module communicates using JSON format API provided in OpenAPI format in the [project repository](https://gitlab.com/pimcity/wp5/open-api/-/blob/master/WP3/DVTMP.yaml). The module first identifies the marketing platform targeted in the request. Then, the corresponding platform submodule analyses the request's parameters. Since each marketing platform has its peculiarities regarding the offered information and how to access it (e.g., search for ids, audiences, or prices), their submodules also independently.

## Installation
The following steps must be taken to install the stand-alone module:

1. Download and install Docker Desktop using the following link address [https://www.docker.com/products/docker-desktop](https://www.docker.com/products/docker-desktop).

    * Windows users should primarily install the Linux subsystem (WSL) to use Docker Desktop. The instructions are available in the following link address: [https://docs.microsoft.com/en-us/windows/wsl/install](https://docs.microsoft.com/en-us/windows/wsl/install).

2. Download (Clone) DVTMP from the project repository using the following link address: [https://gitlab.com/pimcity/wp3/dvtmp](https://gitlab.com/pimcity/wp3/dvtmp)

3. Open a terminal/shell/cmd in the module home folder directory

4. Navigate to the project root directory

5. Make the environment variables (``` config.env ```) ready by adding the Facebook or linkedIn account credentials

6. Run the following command to run the docker container. This command creates a local server located at port 3700. 
```$ docker-compose up```
   * Linux users should run this command using sudo permission ( ```$ sudo docker-compose up``` ) 

## Usage
The installed module communicates using POST API sent to the server located. Following is an example of a query to the server getting the estimated prices in CPM (Cost Per Million Ads. impression) for targeting "female," between 18 to 50 years old, interested in "Travel" who live in "Italy" in the Facebook platform.

Using Python:


The server is located on port 3700 by default and this address can be changed in "docker-compose.yml"

The server receives a POST request with an audience description and responses with a value estimation using Facebook, Instagram and LinkedIn platforms. The following lines of code ask for an estimation of the value, expressed in Cost Per Mille impressions, of an audience in Instagram composed of women between 18 and 50 living in Italy and interested in travelling:

```python
import json
import requests

url = '127.0.0.1:3700/auValuator'

body = {
    "min_age" : 18,
    "max_age": 50,
    "interest": ["Travel"],
    "location": ["IT"],
    "platform":"instagram",
    "priceType":"CPM",
    "gender":"female"
}

r=requests.post(url, data=json.dumps(body))
```

The response (r) will be in the following form:

```python
{
    "status": "success",
    "data":{
        "audience_validated": {
            "max_age": 50,
            "min_age": 18,
            "interest": [
                "Travel"
            ],
            "location": "IT"
        },
        "Platform": "instagram",
        "info": {
            "title": "Data Valuation tool Market Perspective",
            "version": "1.1.0"
        },
        "tags": {
            "name": "auValuator",
            "description": "realtime value"
        },
        "value": 2.276400888218196,
        "currnecy": "USD"
    }
}
```
## Changes
The DVTMP in the new update has implemented the authorization controller module using Keycloak for communication with the other modules. On top of that, the module can now provide estimated prices for advertising on the LinkedIn platform.

# License

The DVTMP is distributed under AGPL-3.0-only, see the `LICENSE` file.

DVTMP
Copyright (C) 2021 UC3M - Amir Mehrjoo, Rubén Cuevas Rumín, Ángel Cuevas Rumín
contact: amir.mehrjoo@imdea.org

```

```
