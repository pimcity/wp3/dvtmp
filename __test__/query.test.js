const request = require("supertest");
const app = require("../app");

it("server is up", (done) => {
  request(app).get("/auValuator/health").expect(200, done);
});

describe("FB test", () => {
  const data = {
    gender: "male",
    interest: ["Engineering"],
    location: "ES",
    max_age: 64,
    min_age: 16,
    platform: "facebook",
    priceType: "CPM",
  };

  test("should return 200 or 201", async () => {
    var response = await request(app).post("/auValuator/").send(data);
    console.log(response.statusCode);
    console.log(response.body.data.value);
    expect(response.status == 201 || response.status == 200);
  });
});
