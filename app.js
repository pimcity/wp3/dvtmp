const path = require("path");
const express = require("express");
const morgan = require("morgan");
const globalErrorHandler = require("./controllers/errorController");
const dotenv = require("dotenv");
// const keycloak = require('./controllers/authController.js').initKeycloak();
const AppError = require("./utils/appError");
const estimationRout = require("./routes/estimationRout");
const viewRouter = require("./routes/viewRoutes");
const healthRouter = require("./routes/healthRout");
const app = express();
const mongoose = require("mongoose");
const ldc = require("./platformHandler/LinkedIn/index");
const fbc = require("./platformHandler/FB/session_init");
dotenv.config({ path: "./config.env" });
// const { exec } = require("child_process");
const fs = require("fs");
const mailer = require("./utils/mail_report");

let DB;
if (process.env.NODE_ENV === "dev_local") {
    DB = process.env.DATABASE_LOCAL_DEV_DEV;
} else {
    DB = process.env.DATABASE_LOCAL;
}

try {
    process.env.FB_COOKIE = fs.readFileSync("FB.cnf", "utf-8");
} catch (error) {}
try {
    fbc.tokenWriter();
} catch (error) {
    console.log(error);
} finally {
    try {
        ldc.accountIdWriter("en_US");
    } catch (error) {
        console.log(error);
    }
}
mongoose
    .connect(DB, {
        useNewUrlParser: true,
        useCreateIndex: true,
        useFindAndModify: false,
    })
    .then(() => console.log("DB connection successful!", process.env.NODE_ENV))
    .catch((error) => {
        console.error(error);
    });

process.on("unhandledRejection", (err) => {
    console.log("UNHANDLED REJECTION! Shutting down...");
    console.log(err.name, err.message);
    mailer(err.name + err.message);
});

if (process.env.NODE_ENV === "development") {
    app.use(morgan("dev"));
}
app.use(express.json());
// app.use(keycloak.middleware());
app.set("view engine", "pug");
app.set("views", path.join(__dirname, "views"));
app.use(express.static(path.join(__dirname, "public")));

app.use((req, res, next) => {
    req.requestTime = new Date().toISOString();
    next();
});

app.use("/auValuator/demo", viewRouter);
app.use("/auValuator", estimationRout);
app.use("/auValuator/health", healthRouter);

app.all("*", (req, res, next) => {
    next(new AppError(`Can't find ${req.originalUrl} on DVTM`, 404));
});
app.all("/auValuator/*", (req, res, next) => {
    next(new AppError(`Can't find ${req.originalUrl} on DVTM`, 404));
});
app.use(globalErrorHandler);

module.exports = app;