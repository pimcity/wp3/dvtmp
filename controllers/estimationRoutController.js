const dotenv = require("dotenv");
const { model } = require("mongoose");
dotenv.config({ path: "./config.env" });

const Audience = require("../models/audienceModel");
const FB = require("../platformHandler/FB/FB");
const LinkedIn = require("../platformHandler/LinkedIn/LinkedIn");
var countries = require("i18n-iso-countries");

exports.plaformselector = async(req, res) => {
    try {
        const auDb = await Audience.findOne(req.body);

        res.status(200).json({
            status: "success",
            data: {
                audience_validated: {
                    max_age: auDb.max_age,
                    min_age: auDb.min_age,
                    interest: auDb.interest,
                    location: auDb.location,
                },
                Platform: auDb.platform,
                info: {
                    title: "Data Valuation tool Market Perspective",
                    version: "2.1.0",
                },
                tags: {
                    name: "auValuator",
                    description: "operation in the local database",
                },
                value: auDb.value,
                currnecy: auDb.currency,
            },
        });
    } catch (err) {
        switch (req.body.platform) {
            case "facebook":
            case "instagram":
                try {
                    const handler = new FB(
                        req.body.interest,
                        req.body.location,
                        req.body.gender,
                        req.body.min_age,
                        req.body.max_age,
                        req.body.priceType,
                        req.body.platform
                    );
                    handler.ids = await handler.getter(handler.interest);
                    req.body.value = await handler.priceEstimator();
                    const newAudience = await Audience.create(req.body);
                    res.status(201).json({
                        status: "success",
                        data: {
                            audience_validated: {
                                max_age: newAudience.max_age,
                                min_age: newAudience.min_age,
                                interest: newAudience.interest,
                                location: newAudience.location,
                            },
                            Platform: newAudience.platform,
                            info: {
                                title: "Data Valuation tool Market Perspective",
                                version: "2.1.0",
                            },
                            tags: {
                                name: "auValuator",
                                description: "realtime value",
                            },
                            value: newAudience.value,
                            currnecy: newAudience.currency,
                        },
                    });
                } catch (err) {
                    res.status(400).json({
                        status: "fail",
                        message: err,
                    });
                }
                break;
            case "linkedIn":
                try {
                    const handler = new LinkedIn(
                        process.env.LinkedIn_ID,
                        process.env.LinkedIn_pass,
                        req.body.interest,
                        req.body.location.map(element => countries.getName(element, "en")),
                        req.body.gender,
                        req.body.min_age,
                        req.body.max_age,
                        req.body.priceType
                    );
                    req.body.value = await handler.priceEstimator();
                    const newAudience = await Audience.create(req.body);
                    res.status(201).json({
                        status: "success",
                        data: {
                            audience_validated: {
                                max_age: newAudience.max_age,
                                min_age: newAudience.min_age,
                                interest: newAudience.interest,
                                location: newAudience.location,
                            },
                            Platform: newAudience.platform,
                            info: {
                                title: "Data Valuation tool Market Perspective",
                                version: "1.1.0",
                            },
                            tags: {
                                name: "auValuator",
                                description: "realtime value",
                            },
                            value: newAudience.value,
                            currnecy: newAudience.currency,
                        },
                    });

                    // res.status(201).json({
                    //   status: 'success',
                    //   data: {
                    //     tags: {
                    //       name: 'auValuator',
                    //       description: 'realtime value',
                    //     },
                    //     value: val,
                    //   },
                    // });
                } catch (err) {
                    res.status(400).json({
                        status: "fail",
                        message: err,
                    });
                }
                break;
        }
    }
};