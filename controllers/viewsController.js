exports.getAuForm = (req, res) => {
  res.status(200).render('demo', {
    title: 'DVTMP Demo',
  });
};
exports.getMain = (req, res) => {
  res.status(200).render('dvtmp', {
    title: 'DVTMP',
  });
};
