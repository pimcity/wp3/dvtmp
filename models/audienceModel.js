const mongoose = require("mongoose");
// const validator = require('validator');

const audienceSchema = new mongoose.Schema({
    platform: {
        type: String,
        enum: ["facebook", "linkedIn", "instagram"],
        default: "facebook",
    },
    gender: {
        type: String,
        enum: ["male", "female", "all"],
        default: "all",
    },
    priceType: {
        type: String,
        enum: ["CPM", "CPC"],
        default: "CPM",
    },
    min_age: {
        type: Number,
        min: [13, "the age should be between 13 and 65"],
        max: [65, "the age should be between 13 and 65"],
    },
    max_age: {
        type: Number,
        min: [13, "the age should be between 13 and 65"],
        max: [65, "the age should be between 13 and 65"],
    },
    interest: [{
        type: String,
    }, ],
    location: [{
        type: String,
        required: [true, "location is required (e.g. ES)"],
    }, ],
    value: {
        type: Number,
    },
    currency: {
        type: String,
        default: "USD",
    },
    queryDate: {
        type: Date,
        default: Date.now(),
    },
});
const Audience = mongoose.model("Audience", audienceSchema);
module.exports = Audience;