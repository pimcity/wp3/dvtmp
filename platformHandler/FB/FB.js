const requestp = require("request-promise");
const axios = require("axios");
const idCatcher = require("./idCatcher");
const idCatcher2 = require("./idCatcher2");

class FBplatformHandler {
    constructor(interest, country, gender, ageMin, ageMax, priceType, platform) {
        this.ckk = process.env.FB_COOKIE;
        this.interest = interest;
        this.country = country;
        this.gender = gender;
        this.min_age = ageMin;
        this.max_age = ageMax;
        this.priceType = priceType;
        this.point = 14;
        this.platform = platform;
        this.ids = [];
    }

    async reqID() {
        const url = `https://graph.facebook.com/v12.0/act_${
      this.ckk.split(",")[0]
    }/targetingsearch?access_token=${this.ckk.split(",")[1]}&locale=en_US&q=${
      this.interest
    }&type=adinterest&li`;
        var headers2 = {};
        var cookies = this.ckk.split(",")[2];
        headers2["cookie"] = cookies;
        headers2["host"] = "graph.facebook.com";

        var options = { url: url, method: "GET", headers: headers2 };
        const results = await requestp(options);
        const id_details = JSON.parse(results).data["0"];
        this.id = id_details.id;
        console.log(
            `${this.platform} can reach ${id_details.audience_size} audiences interested in ${id_details.name}`
        );
        return String(this.id);
    }

    async genderprep() {
        if (this.gender === "male") {
            this.numgender = "1";
        } else if (this.gender === "female") {
            this.numgender = "2";
        } else {
            this.numgender = "0";
        }
        return this.numgender;
    }

    async getter(list) {
        const ids = await Promise.all(
            list.map(async(item) => {
                if (item && item != "0") {
                    return await idCatcher(item, this.ckk, 0);
                } else {
                    return "0";
                }
            })
        );
        // const validIds = ids.filter((item) => item != "000");
        // console.log("valids are :  " + validIds);
        return ids;
    }
    async priceEstimator() {
        if (this.priceType === "CPC") {
            this.optimizationGoal = "LINK_CLICKS";
        } else {
            this.optimizationGoal = "IMPRESSIONS";
        }

        const beginingPart = `https://graph.facebook.com/v12.0/act_`;
        const loggingPart = `${
      this.ckk.split(",")[0]
    }/delivery_estimate?access_token=${this.ckk.split(",")[1]}`;
        const goalPart = `&optimization_goal=${this.optimizationGoal}&`;
        let agemaxPart = `"age_max":${String(this.max_age)},`;
        let ageminPart = `"age_min":${String(this.min_age)},`;
        let genderPart = `"genders":[${await this.genderprep()}],`;
        let platformPart = `,"publisher_platforms": ["${this.platform}"]}`;
        const geolocationPart = `"geo_locations":{"countries":["${this.country.join(
      '","'
    )}"],"location_types":["home"]}`;

        let interestPart = `,"interests": [{ "id": "${this.ids.join(
      '"}, { "id": "'
    )}"}]`;
        if (this.gender === "0") {
            genderPart = "";
        }
        if (this.max_age === "MAX") {
            agemaxPart = "";
        }
        if (this.min_age === "MIN") {
            ageminPart = "";
        }
        if (this.interest[0] == "0" || !this.interest[0]) {
            interestPart = "";
        }
        var url = `${beginingPart}${loggingPart}${goalPart}targeting_spec={${ageminPart}${agemaxPart}${genderPart}${geolocationPart}${interestPart}${platformPart}`;
        var headers2 = {};

        var cookies = this.ckk.split(",")[2] + "; oo=v1; dpr=1.75; locale=en_US";
        headers2["cookie"] = cookies;
        headers2["host"] = "graph.facebook.com";
        var options = { url: url, method: "GET", headers: headers2 };
        var counter = 0;

        while (!this.results && counter < 7) {
            console.log(this.results);
            counter++;
            try {
                console.log(this.ids);
                this.results = await axios.request(options);
            } catch (err) {
                const faulty = err.response.data.error.message
                    .split("targeting_spec[interests][")[1]
                    .split("][id] must be a valid interest id")[0];
                console.log(`${this.interest[faulty]} is faulty`);
                this.ids.splice(faulty, 1);
                this.ids.push(await idCatcher2(this.interest[faulty], this.ckk, 0));
                console.log(`we replaced with ${this.ids[faulty]}`);

                interestPart = `,"interests": [{ "id": "${this.ids.join(
          '"}, { "id": "'
        )}"}]`;
                url = `${beginingPart}${loggingPart}${goalPart}targeting_spec={${ageminPart}${agemaxPart}${genderPart}${geolocationPart}${interestPart}${platformPart}`;

                options = { url: url, method: "GET", headers: headers2 };
            }
        }
        this.point = this.results.data.data[0].daily_outcomes_curve.length - 1;

        this.cost =
            this.results.data.data[0].daily_outcomes_curve[this.point].spend;
        this.impressions =
            this.results.data.data[0].daily_outcomes_curve[this.point].impressions;
        this.cpm = (this.cost * 1000) / this.impressions;
        console.log(this.cpm);
        return this.cpm / 100;
    }
}

module.exports = FBplatformHandler;