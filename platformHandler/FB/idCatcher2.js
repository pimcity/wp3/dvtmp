const requestp = require("request-promise");

module.exports = async(interest, ckk, item) => {
    const url = `https://graph.facebook.com/v12.0/search?access_token=${
    ckk.split(",")[1]
  }&locale=en_US&interest_list=["${
    interest.split(" ")[0]
  }"]&type=adinterestsuggestion`;
    // console.log(url);
    var headers2 = {};
    var cookies = ckk.split(",")[2];
    headers2["cookie"] = cookies;
    headers2["host"] = "graph.facebook.com";

    var options = { url: url, method: "GET", headers: headers2 };
    const results = await requestp(options);
    return JSON.parse(results).data[String(item)].id;
};