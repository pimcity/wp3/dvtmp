const puppeteer = require("puppeteer");
const mailer = require("../../utils/mail_report");

var page;
const fs = require("fs");

function sleep(ms) {
    return new Promise((resolve) => {
        setTimeout(resolve, ms);
    });
}
async function initFBSession(id, pass) {
    let browser;

    let puppeteerOptions = {
        headless: true,
        args: [
            "--window-size=1270x950",
            "--disable-crash-reporter",
            "--lang=en-EN,en",
            "--no-sandbox",
            "--disable-setuid-sandbox",
        ],
        defaultViewport: null,
    };

    try {
        browser = await puppeteer.launch(puppeteerOptions);

        page = await browser.newPage();
        await page.setDefaultNavigationTimeout(0);
        await page.setUserAgent(
            "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3419.0 Safari/537.36"
        );
        await page
            .goto("https://www.facebook.com/ads/manager")
            .catch(function gotoError(error) {
                console.log("Campaign Manager Login window" + error);
            });

        var button_FB = await page.$('button[data-cookiebanner="accept_button"]');

        await button_FB.click();
        await page.waitForTimeout(1000);

        await page.type("input#email", id, { delay: 10 });
        await page.type("input#pass", pass, { delay: 10 });
        button_FB = await page.$('button[name="login"]');
        await button_FB.click();

        await page.setRequestInterception(true);
        var tokenandid = await getTokenFB(page);
        await page.close();
        await browser.close();
    } catch (err) {
        console.log(err);
        await page.close();
        await browser.close();
    }
    return tokenandid;
}

async function getCookies(page) {
    var cookies = await page.cookies();
    cookies = Array.prototype.slice.call(cookies);
    cookies = cookies
        .map((cookie) => {
            return `${cookie.name}=${cookie.value}`;
        })
        .join("; ");
    return cookies;
}

async function getTokenFB(page) {
    var url, index, accountId_part, token;

    page.on("request", (interceptedRequest) => {
        url = interceptedRequest.url();
        filter = /access_token=[\s\S]*&__cppo/g;
        index = url.match(filter);
        if (index) {
            accountId_part =
                url.match(/act_\d*/g)[0].split("act_")[1] || account_Id_part;
            token =
                url
                .match(/access_token=[\s\S]*&__cppo/g)[0]
                .split("access_token=")[1]
                .split("&__cppo")[0] || token;
        }
        interceptedRequest.continue();
    });

    await page.reload({ waitUntil: ["networkidle2"] });

    cookies = await getCookies(page);
    console.log(accountId_part.toString(), token.toString());

    return [accountId_part.toString(), token.toString(), cookies];
}
var creds;
async function tokenWriter() {
    while (true) {
        var count = 0;
        const ids = [process.env.FB_ACID, process.env.FB_ACID2];
        const passes = [process.env.FB_PSS, process.env.FB_PSS2];
        creds = false;
        var i = 0;
        while (!creds) {
            count++;
            if (count % 2 == 0) {
                i = 1;
            } else {
                i = 0;
            }
            try {
                creds = await initFBSession(ids[i], passes[i]);
                if (!creds) {
                    console.log(`Retried FB session for ${count} times`);
                    if (count > 7) {
                        mailer(`Retried FB session for ${count} times`);
                    }

                    await sleep(20000);
                }
            } catch (error) {}
        }

        await fs.writeFile("FB.cnf", creds.toString(), function(err, result) {
            process.env.FB_COOKIE = creds.toString();
            if (err) console.log("error", err);
        });

        console.log("FB ready");
        await sleep(43200000);
    }
}
exports.tokenWriter = tokenWriter;