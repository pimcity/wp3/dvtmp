// const puppeteer = require("puppeteer");
const ldc = require("./index");
class LinkedInPlatformHandler {
  constructor(ID, pass, interest, country, gender, ageMin, ageMax, priceType) {
    this.user = {
      username: ID,
      password: pass,
      userDataDir: "chromium/devDataDir",
    };


    this.priceType = priceType;
    (this.gender = gender),
    (this.ages = [ageMin, ageMax]),
    //(this.ages = `${ageMin} to ${ageMax}`),
    (this.ct = country),
    (this.interest = interest);
  }

  async priceEstimator() {

    //Age ranges parsing algorithm: searches for the age group combination closer to the given range.

    var ageMins = [18,25,35,55];
    var ageMaxs = [24,34,54,55];

    const ageRanges = ['18 to 24',
		       '25 to 34',
		       '35 to 54',
                       '55+'];

    var ageRangesToInclude = [];
    //1:Calculate distances of ageMin/ageMax with available ageMins and ageMaxs in LD

    var distMin = ageMins.map(element => Math.abs(element - this.ages[0]));
    var distMax = ageMaxs.map(element => Math.abs(element - this.ages[1]));

    this.ages[0] = ageMins[distMin.indexOf(Math.min(...distMin))];
    this.ages[1] = ageMaxs[distMax.indexOf(Math.min(...distMax))];

    var nRanges = ageMaxs.indexOf(this.ages[1]) - ageMins.indexOf(this.ages[0]) + 1;

    var firstRange = ageMins.indexOf(this.ages[0]);

    for(let i=0; i<nRanges; i++){
    	ageRangesToInclude.push(ageRanges[firstRange+i]);
    }

    let search = await ldc.getURN(this.interest,['skills','interests','employers']);
    if(search[0] == -3){
      this.interest = this.interest.map(element => element.split(" ")[0].slice(0,-2));
      await ldc.getURN(this.interest,['skills','interests','employers']);
    }
    console.log(this.ct.map(element => element.toLowerCase()));
    console.log(typeof this.ct);
    var dataArray2d = [this.ct.map(element => element.toLowerCase()), this.interest, [this.gender.toLowerCase()], ageRangesToInclude];

    var cpc = await ldc.getBidSuggestion(dataArray2d, "CPC");
    var cpm = await ldc.getBidSuggestion(dataArray2d, "CPM");

    console.log("CPC: " + cpc + ", CPM: " + cpm);
    if (this.priceType === "CPM") {
      return cpm;
    } else {
      return cpc;
    }
  }
  catch(err) {
    console.log(err);
    process.exit(1);
  }
}
module.exports = LinkedInPlatformHandler;
// ranges: 18- 24, 25-34, 35-54, 55+
