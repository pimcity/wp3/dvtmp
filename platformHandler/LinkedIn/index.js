//GLOBAL VARIABLES
const path = "./";

const fs = require("fs");
const request = require("request");
const puppeteer = require("puppeteer");
const mailer = require("../../utils/mail_report");

var headers;
var page;

const arr_in = [
    "locations",
    "titles",
    "industries",
    "genders",
    "ageRanges",
    "jobFunctions",
    "skills",
    "employers",
    "degrees",
    "schools",
    "titles",
    "groups",
    "interests",
    "fieldsOfStudy",
    "ageRanges",
];

const arr_out = [
    "geo",
    "title",
    "industry",
    "gender",
    "ageRange",
    "function",
    "skill",
    "company",
    "degree",
    "school",
    "title",
    "group",
    "interest",
    "fieldOfStudy",
    "ageRange",
];

const dict_in = {
    locations: "Ubicaciones",
    titles: "Cargos",
    industries: "Company%20Industries",
    genders: "Member%20Gender",
    ageRanges: "Member%20Age",
    jobFunctions: "JobFunctions",
    skills: "Aptitudes",
    employers: "Nombres%20de%20empresas",
    degrees: "Titulaciones",
    schools: "Universidades",
    titles: "Cargos",
    groups: "Grupos",
    interests: "Intereses%20de%20los%20miembros",
    fieldsOfStudy: "Disciplinas%20acad%C3%A9micas",
};

function doRequest(options) {
    return new Promise(function(resolve, reject) {
        request(options, function(error, res, body) {
            if (!error && res.statusCode == 200) {
                resolve(body);
            } else {
                if (error) {
                    console.log(options.body);
                    reject(error);
                } else {
                    console.log(
                        "Error at doRquest (status " +
                        res.statusCode +
                        "). Showing response body: \n",
                        JSON.stringify(body)
                    );
                    try {
                        console.log(JSON.stringify(options.body));
                    } catch (err) {
                        console.log(options.body);
                    }
                }
            }
        });
    });
}

function requestedComb(dataArray2d) {
    var requested = "";
    for (let i = 0; i < dataArray2d.length; i++) {
        if (i != 0) requested += " AND ";
        for (let j = 0; j < dataArray2d[i].length; j++) {
            if (j == 0) requested += "(";
            requested += dataArray2d[i][j];
            if (j < dataArray2d[i].length - 1) requested += " OR ";
            else requested += ")";
        }
    }
    return requested;
}

function checkGeo(dataArray2d, json_urns) {
    let containsGeo = false;
    for (i in dataArray2d) {
        // check for some geo in dataArray2d
        for (j in dataArray2d[i]) {
            try {
                if (json_urns[dataArray2d[i][j]].type == "geo") containsGeo = true;
            } catch (err) {
                for (k in json_urns) {
                    if (
                        json_urns[k].otherNames.includes(dataArray2d[i][j]) &&
                        json_urns[k].type == "geo"
                    )
                        containsGeo = true;
                }
            }
        }
    }
    return containsGeo;
}

function getTargetingCriteria(dataArray2d, json_urns) {
    preamb = "(include:(and:List(";
    epilogue = ",exclude:(or:List()))";
    lang =
        "(or:List((facet:(urn:urn%3Ali%3AadTargetingFacet%3AinterfaceLocales,name:Idiomas%20de%20la%20interfaz),segments:List((urn:urn%3Ali%3Alocale%3Aen_US,name:Ingl%C3%A9s,facetUrn:urn%3Ali%3AadTargetingFacet%3AinterfaceLocales)))))";
    cmTargetingCriteria = preamb + lang;

    for (let i = 0; i < dataArray2d.length; i++) {
        counter = 0;
        for (let j = 0; j < dataArray2d[i].length; j++) {
            uncodedName = dataArray2d[i][j].toLowerCase();
            try {
                urn = encodeURIComponent(json_urns[uncodedName].urn)
                    .replace(/[!'()]/g, escape)
                    .replace(/\*/g, "%2A");
                typeOut = json_urns[uncodedName].type;
                name = encodeURIComponent(uncodedName)
                    .replace(/[!'()]/g, escape)
                    .replace(/\*/g, "%2A");
                ancestorList = json_urns[uncodedName].ancestorList;
            } catch (err) {
                for (k in json_urns) {
                    if (json_urns[k].otherNames.includes(uncodedName)) {
                        urn = encodeURIComponent(json_urns[k].urn)
                            .replace(/[!'()]/g, escape)
                            .replace(/\*/g, "%2A");
                        typeOut = json_urns[k].type;
                        ancestorList = json_urns[k].ancestorList;
                        name = encodeURIComponent(k)
                            .replace(/[!'()]/g, escape)
                            .replace(/\*/g, "%2A");
                        break;
                    }
                }
            }
            counter++;

            typeIn = arr_out.indexOf(typeOut);
            let differentType = false;
            if (j != 0) {
                if (previousType != typeOut) differentType = true;
            }

            firstOr =
                ",(or:List((facet:(urn:urn%3Ali%3AadTargetingFacet%3A" +
                arr_in[typeIn] +
                ",name:" +
                dict_in[arr_in[typeIn]] +
                "),segments:List((urn:urn%3Ali%3A" +
                arr_out[typeIn] +
                "%3A" +
                urn +
                ",name:" +
                name +
                ",facetUrn:urn%3Ali%3AadTargetingFacet%3A" +
                arr_in[typeIn] +
                ")";
            middleOr =
                ",(urn:urn%3Ali%3A" +
                arr_out[typeIn] +
                "%3A" +
                urn +
                ",name:" +
                name +
                ",facetUrn:urn%3Ali%3AadTargetingFacet%3A" +
                arr_in[typeIn] +
                ")";
            middleOrDifferentType =
                ")),(facet:(urn:urn%3Ali%3AadTargetingFacet%3A" +
                arr_in[typeIn] +
                ",name:" +
                dict_in[arr_in[typeIn]] +
                "),segments:List((urn:urn%3Ali%3A" +
                arr_out[typeIn] +
                "%3A" +
                urn +
                ",name:" +
                name +
                ",facetUrn:urn%3Ali%3AadTargetingFacet%3A" +
                arr_in[typeIn] +
                ")";
            lastOr =
                ",(urn:urn%3Ali%3A" +
                arr_out[typeIn] +
                "%3A" +
                urn +
                ",name:" +
                name +
                ",facetUrn:urn%3Ali%3AadTargetingFacet%3A" +
                arr_in[typeIn] +
                ")))))";
            lastOrDifferentType =
                ")),(facet:(urn:urn%3Ali%3AadTargetingFacet%3A" +
                arr_in[typeIn] +
                ",name:" +
                dict_in[arr_in[typeIn]] +
                "),segments:List((urn:urn%3Ali%3A" +
                arr_out[typeIn] +
                "%3A" +
                urn +
                ",name:" +
                name +
                ",facetUrn:urn%3Ali%3AadTargetingFacet%3A" +
                arr_in[typeIn] +
                ")))))";

            uniqueOr =
                ",(or:List((facet:(urn:urn%3Ali%3AadTargetingFacet%3A" +
                arr_in[typeIn] +
                ",name:" +
                dict_in[arr_in[typeIn]] +
                "),segments:List((urn:urn%3Ali%3A" +
                arr_out[typeIn] +
                "%3A" +
                urn +
                ",name:" +
                name +
                ",facetUrn:urn%3Ali%3AadTargetingFacet%3A" +
                arr_in[typeIn] +
                ")))))";

            firstOrWithAncestor =
                ",(or:List((facet:(urn:urn%3Ali%3AadTargetingFacet%3A" +
                arr_in[typeIn] +
                ",name:" +
                dict_in[arr_in[typeIn]] +
                "),segments:List((urn:urn%3Ali%3A" +
                arr_out[typeIn] +
                "%3A" +
                urn +
                ",name:" +
                name +
                ",facetUrn:urn%3Ali%3AadTargetingFacet%3A" +
                arr_in[typeIn] +
                ",ancestorUrns:List(";
            for (let k = 0; k < ancestorList.length; k++) {
                firstOrWithAncestor += "urn%3Ali%3Ageo%3A" + ancestorList[k];
                if (k != ancestorList.length - 1) firstOrWithAncestor += ",";
            }
            firstOrWithAncestor += "))";

            middleOrWithAncestor =
                ",(urn:urn%3Ali%3A" +
                arr_out[typeIn] +
                "%3A" +
                urn +
                ",name:" +
                name +
                ",facetUrn:urn%3Ali%3AadTargetingFacet%3A" +
                arr_in[typeIn] +
                ",ancestorUrns:List(";
            middleOrWithAncestorDifferentType =
                ")),(facet:(urn:urn%3Ali%3AadTargetingFacet%3A" +
                arr_in[typeIn] +
                ",name:" +
                dict_in[arr_in[typeIn]] +
                "),segments:List((urn:urn%3Ali%3A" +
                arr_out[typeIn] +
                "%3A" +
                urn +
                ",name:" +
                name +
                ",facetUrn:urn%3Ali%3AadTargetingFacet%3A" +
                arr_in[typeIn] +
                ",ancestorUrns:List(";
            for (let k = 0; k < ancestorList.length; k++) {
                middleOrWithAncestor += "urn%3Ali%3Ageo%3A" + ancestorList[k];
                if (k != ancestorList.length - 1) middleOrWithAncestor += ",";
                middleOrWithAncestorDifferentType +=
                    "urn%3Ali%3Ageo%3A" + ancestorList[k];
                if (k != ancestorList.length - 1)
                    middleOrWithAncestorDifferentType += ",";
            }
            middleOrWithAncestor += "))";
            middleOrWithAncestorDifferentType += "))";

            lastOrWithAncestor =
                ",(urn:urn%3Ali%3A" +
                arr_out[typeIn] +
                "%3A" +
                urn +
                ",name:" +
                name +
                ",facetUrn:urn%3Ali%3AadTargetingFacet%3A" +
                arr_in[typeIn] +
                ",ancestorUrns:List(";
            lastOrWithAncestorDifferentType =
                ")),(facet:(urn:urn%3Ali%3AadTargetingFacet%3A" +
                arr_in[typeIn] +
                ",name:" +
                dict_in[arr_in[typeIn]] +
                "),segments:List((urn:urn%3Ali%3A" +
                arr_out[typeIn] +
                "%3A" +
                urn +
                ",name:" +
                name +
                ",facetUrn:urn%3Ali%3AadTargetingFacet%3A" +
                arr_in[typeIn] +
                ",ancestorUrns:List(";
            for (let k = 0; k < ancestorList.length; k++) {
                lastOrWithAncestor += "urn%3Ali%3Ageo%3A" + ancestorList[k];
                if (k != ancestorList.length - 1) lastOrWithAncestor += ",";
                lastOrWithAncestorDifferentType +=
                    "urn%3Ali%3Ageo%3A" + ancestorList[k];
                if (k != ancestorList.length - 1)
                    lastOrWithAncestorDifferentType += ",";
            }
            lastOrWithAncestor += "))))))";
            lastOrWithAncestorDifferentType += "))))))";

            uniqueOrWithAncestor =
                ",(or:List((facet:(urn:urn%3Ali%3AadTargetingFacet%3A" +
                arr_in[typeIn] +
                ",name:" +
                dict_in[arr_in[typeIn]] +
                "),segments:List((urn:urn%3Ali%3A" +
                arr_out[typeIn] +
                "%3A" +
                urn +
                ",name:" +
                name +
                ",facetUrn:urn%3Ali%3AadTargetingFacet%3A" +
                arr_in[typeIn] +
                ",ancestorUrns:List(";
            for (let k = 0; k < ancestorList.length; k++) {
                uniqueOrWithAncestor += "urn%3Ali%3Ageo%3A" + ancestorList[k];
                if (k != ancestorList.length - 1) uniqueOrWithAncestor += ",";
            }
            uniqueOrWithAncestor += "))))))";

            if (dataArray2d[i].length - 1 == 0) {
                if (ancestorList.length > 0)
                    cmTargetingCriteria += uniqueOrWithAncestor;
                else cmTargetingCriteria += uniqueOr;
            } else {
                if (j == 0) {
                    if (ancestorList.length > 0)
                        cmTargetingCriteria += firstOrWithAncestor;
                    else cmTargetingCriteria += firstOr;
                }
                if (j == dataArray2d[i].length - 1) {
                    if (ancestorList.length > 0) {
                        if (differentType)
                            cmTargetingCriteria += lastOrWithAncestorDifferentType;
                        else cmTargetingCriteria += lastOrWithAncestor;
                    } else {
                        if (differentType) cmTargetingCriteria += lastOrDifferentType;
                        else cmTargetingCriteria += lastOr;
                    }
                } else if (j > 0) {
                    if (ancestorList.length > 0) {
                        if (differentType)
                            cmTargetingCriteria += middleOrWithAncestorDifferentType;
                        else cmTargetingCriteria += middleOrWithAncestor;
                    } else {
                        if (differentType) cmTargetingCriteria += middleOrDifferentType;
                        else cmTargetingCriteria += middleOr;
                    }
                }
            }
            previousType = typeOut;
        }
    } //fin de los dos for

    cmTargetingCriteria += "))" + epilogue;
    return cmTargetingCriteria;
}

function loadReg() {
    try {
        let data = fs.readFileSync("json_urns.json", "utf-8");
        json_urns = JSON.parse(data);
    } catch (err) {
        console.log("Error at loadReg: " + err);
        json_urns = {};
    }
    return json_urns;
}

function checkReg(dataArraySimple,json_urns){
  let toSearch = [];
  for(let i in dataArraySimple){
    let exists = false;
    for(let element in json_urns){
      if(dataArraySimple[i].toLowerCase() == element.toLowerCase()){
        exists = true;
      }
      else{
        for(let j in json_urns[element].otherNames){
          if(json_urns[element].otherNames[j].toLowerCase() == dataArraySimple[i].toLowerCase()){
            exists = true;
          }
        }
      }
    }
    if(!exists) toSearch.push(dataArraySimple[i]);
  }
  return toSearch;
}

function updateReg(salidaJSON, json_urns){
  for (let i in salidaJSON) {
    let exists = false;
    for (let j in json_urns) {
      if (json_urns[j].urn == salidaJSON[i].urn && json_urns[j].type == salidaJSON[i].type) {
        exists = true;
	if(j != i){
          if(!json_urns[j].otherNames.includes(i)) json_urns[j].otherNames = json_urns[j].otherNames.concat(i);
        }
        json_urns[j].otherNames = [... new Set(json_urns[j].otherNames.concat(salidaJSON[i].otherNames))];
        json_urns[j].otherNames = json_urns[j].otherNames.filter(item => item !== j);
      }
    }
    if(exists == false) json_urns[i] = salidaJSON[i];
  }
  fs.writeFileSync(path+'json_urns.json',JSON.stringify(json_urns));
}


function sleep(ms) {
     return new Promise((resolve) => {
        setTimeout(resolve, ms);
    });
}

async function initSession(lang) {
    let browser;
    let useWpr = false;

    let wprArgs = [
        "--ignore-certificate-errors-spki-list=PhrPvGIaAMmd29hj8BCZOq096yj7uMpRNHpn5PDxI6I=",
        '--host-resolver-rules="MAP *:80 127.0.0.1:8080,MAP *:443 127.0.0.1:8081,EXCLUDE localhost"',
    ];

    let user = {
        username: process.env.LINKEDIN_ID,
        password: process.env.LINKEDIN_PSS,
        userDataDir: "chromium/devDataDir",
    };

    let puppeteerOptions = {
        headless: true,
        args: [
            "--window-size=1900x1200",
            "--disable-crash-reporter",
            "--lang=es-ES,es",
            "--no-sandbox",
            "--disable-setuid-sandbox",
        ],
        defaultViewport: null,
    };

    if (useWpr) {
        puppeteerOptions.args = puppeteerOptions.args.concat(wprArgs);
    }
    if (user.userDataDir) {
        puppeteerOptions.userDataDir = user.userDataDir;
    }

    try {
        browser = await puppeteer.launch(puppeteerOptions);
        const context = await browser.createIncognitoBrowserContext();
        page = await context.newPage();
        await page.setDefaultNavigationTimeout(0);
        await page.setUserAgent(
            "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3419.0 Safari/537.36"
        );
        await page
            .goto("https://www.linkedin.com/login")
            .catch(function gotoError(error) {
                console.log("Campaign Manager Login window" + error);
            });

        authFrame = page;
        await page.waitForTimeout(1000);
        await authFrame.$eval("input#password", (element) => (element.value = ""));
        await authFrame.type("input#password", user.password, { delay: 10 });

        await page.waitForTimeout(1000);
        await authFrame.$eval("input#username", (element) => (element.value = ""));
        await authFrame.type("input#username", user.username, { delay: 10 });
        await Promise.all([
            authFrame.click(".btn__primary--large", { delay: 2 }),
            //page.waitForNavigation({ waitUntil: 'networkidle2' }),
            //authFrame.click('.btn__primary--large', { delay: 2 }),
        ]);

        await page.waitForTimeout(20000);
        await page.screenshot({ path: "jd.jpg", fullPage: true });
        button = await page.$("button#compactfooter-more");
        await button.evaluate((b) => b.click());
        await page.waitForTimeout(1000);
        button = await page.select("select#globalfooter-select_language", lang);
        await page.waitForTimeout(3000);
        await page.goto("https://www.linkedin.com/campaignmanager/accounts");

        await page.waitForTimeout(1000);
        [accountId, token] = await getTokenAndId(page);
        cookies = await getCookies(page);
        return [accountId.toString(), token, cookies];
    } catch (err) {
        console.log(err);
        await page.close();
        await browser.close();
    }
}

async function accountIdWriter() {
    while (true) {
        var creds = false;
        var cntr = 0;
        while (!creds) {
            try {
                creds = await initSession("en_US");
            } catch (error) {}
            if (!creds) {
                cntr++;
                console.log(`Retried LD session for ${cntr} times`);
                if (cntr > 7) {
                    mailer(`Retried LD session for ${cntr} times`);
                }
                await sleep(15000);
            }
        }
        fs.writeFile("LD.cnf", creds.toString(), function(err, result) {
            if (err) console.log("error", err);
        });
        console.log("LD ready");

        await sleep(72000000);
    }
}

async function getCookies(page) {
    var cookies = await page.cookies();
    cookies = Array.prototype.slice.call(cookies);
    cookies = cookies
        .map((cookie) => {
            return `${cookie.name}=${cookie.value}`;
        })
        .join("; ");

    return cookies;
}
async function getTokenAndId(page) {
    await page.setRequestInterception(true);
    var counter, url, index, accountId_part, accountId;
    page.on("request", (interceptedRequest) => {
        url = interceptedRequest.url();
        if (interceptedRequest.headers().hasOwnProperty("csrf-token")) {
            headers = interceptedRequest.headers();
            token = headers["csrf-token"]; // Saves csrf-token and other data
        }
        index = url.search("accountId=");
        if (index != -1) {
            accountId_part = url.slice(index + 10);
            index = accountId_part.search("&");
            if (index != -1) {
                accountId = accountId_part.slice(0, index);
            } else {
                accountId = accountId_part;
            }
        }
        counter++;
        interceptedRequest.continue();
    });

    await Promise.all([
        page.waitForNavigation({ waitUntil: "networkidle0" }),
        page.click('a[href*="campaign-groups"]', { delay: 11 }),
    ]);

    await Promise.all([
        page.waitForNavigation({ waitUntil: "networkidle0" }),
        page.click('a[href*="campaigns"]', { delay: 8 }),
    ]);

    await page.setExtraHTTPHeaders(headers);

    return [accountId, token];
}

/**
* [Saves the URNs of the received elements]
* @param  {Object} page [Puppeteer page of current session]
* @param  {[String]} dataArraySimple [Array containing one or more elements]
* @param  {String} accountId [String containing the id of the advertising account]
* @param  {[String]} typesList [Array containing one or more elements of the arr_in list]
*/
async function getURN(dataArraySimple, typesList) {

  if (typesList.length == 0){

    typesList = ['locations','titles','jobFunctions','skills','fieldsOfStudy',
    'degrees','schools','groups','interests','employers'];
  }

  let target, urnAndType, array, name, type, urn, ancestorList, targetType, requestURL, i, j;
  let toSearch = [];
  let salidaJSON = {};
  json_urns = loadReg();

  toSearch = checkReg(dataArraySimple, json_urns); //Returns the items not found in the register

  if(toSearch.length == 0){
    return [-2,'Item already in local file'];
  }
  let accountId = fs.readFileSync("LD.cnf", "utf-8").split(",")[0];

  for(i=0; i< toSearch.length; i++){

    target = encodeURIComponent(toSearch[i]).replace(/[!'()]/g, escape).replace(/\*/g, "%2A");

    for(j = 0; j < typesList.length; j++){
      name = "";
      type = "";
      urn = -1;
      ancestorList = [];
      targetType = typesList[j];
      requestURL = 'https://www.linkedin.com/campaign-manager-api/campaignManagerAdTargetingEntities?query='+target+'&accountId='+accountId+'&facets=List(urn%3Ali%3AadTargetingFacet%3A'+targetType+')&q=queryAndMultiFacetTypeahead';
      var headers2 = {};

      headers2["content-type"] = "application/x-www-form-urlencoded";
      headers2["cookie"] = fs.readFileSync("LD.cnf", "utf-8").split(",")[2];
      headers2["csrf-token"] = fs.readFileSync("LD.cnf", "utf-8").split(",")[1];
      headers2["referer"] =
        "https://www.linkedin.com/campaignmanager/accounts/" +
        accountId +
        "/campaigns/new/details";
      // headers2["user-agent"] = headers["user-agent"];
      headers2["x-http-method-override"] = "GET";
      headers2["x-restli-protocol-version"] = "2.0.0";

      var options = {
          url: requestURL,
          headers: headers2,
      };

      try{
        const response = await doRequest(options);
        var responseBody = await JSON.parse(response);
        var responseBodyJSON = responseBody;

      }catch(err){
        console.log("Error en la petición getURN del item "+target+": "+err);
        console.log(responseBody);
        if(err == "SyntaxError: Unexpected token C in JSON at position 0"){
          console.log(responseBody);
          throw new Error(err);
        }
      }

      try{
        if (responseBodyJSON.elements.length != 0){
          let index = 0;
          for(let k=0; k<responseBodyJSON.elements.length; k++){
            if (toSearch[i].toLowerCase() == responseBodyJSON.elements[k].name.toLowerCase()){
              index = k;
            }
          }
          urnAndType = responseBodyJSON.elements[index].urn;
          array = urnAndType.split(':');
          urn = array[3];
          type = array[2];
          name = responseBodyJSON.elements[index].name.toLowerCase();
          ancestorList = [];
          ancestorUrns = responseBodyJSON.elements[index].ancestorUrns;
          try{
            if(ancestorUrns.length > 0){
              ancestorUrns = ancestorUrns.toString();
              let array2 = ancestorUrns.split(',');
              for(let k = 0; k<array2.length; k++){
                array = array2[k].split(':');
                ancestorList = ancestorList.concat(array[3]);
              }
            }
          }catch(err){
            ancestorList = [];
          }
          if(name.toLowerCase() != toSearch[i].toLowerCase()) salidaJSON[name] = {"otherNames":[toSearch[i].toLowerCase()], "urn":urn, "type":type, "ancestorList":ancestorList};
          else salidaJSON[name] = {"otherNames":[], "urn":urn, "type":type, "ancestorList":ancestorList};
          break;
        }else{
          continue;
        }
      }catch (err){
	console.log(err);
        throw new Error(err);
      }
      if(j == typesList.length-1) console.log("Can't find the item "+target+" at getURN (any type returns a match)")
    }
  }
  console.log(salidaJSON);
  if(salidaJSON == {}){
    return [-3, 'Item not found'];
  }else{
    updateReg(salidaJSON,json_urns);
    return [urn, name];
  }
}

/**
* [Gets the bid suggestion of the received audience and saves the response in suggestion.json file]
* @param  {Object} page [Puppeteer page of current session]
* @param  {[[String]]} dataArray2d [Combination of elements matching the audience]
* @param  {String} accountId [String containing the id of the advertising account]
* @param  {String} bidType [String indicating the bid type.
                            It can take one of this values: 'CPC', 'CPM']
*/
async function getBidSuggestion(dataArray2d, bidType) {
    var form,
        cmTargetingCriteria,
        json_urns,
        requested = "",
        accountId = fs.readFileSync("LD.cnf", "utf-8").split(",")[0];

    json_urns = loadReg();

    if (checkGeo(dataArray2d, json_urns) == false) {
        console.log(
            "Forecasting: El fichero de entrada debe contener al menos una ubicación válida"
        );
        return -1;
    }

    form = "q=criteriaV2";
    form += "&adFormats=List(STANDARD_SPONSORED_CONTENT)";
    form += "&accountId=" + accountId.toString();
    form += "&targetingCriteria=";

    cmTargetingCriteria = getTargetingCriteria(dataArray2d, json_urns);
    form += cmTargetingCriteria;

    form += "&bidType=" + bidType;
    form += "&currency=USD";
    form += "&matchType=EXACT";
    form += "&productType=MARKETING_SOLUTIONS";
    form += "&roadblockType=NONE";
    form += "&optimizationTargetType=NONE";
    form += "&dailyBudget=(amount:50.00,currencyCode:USD)";
    form += "&objectiveType=WEBSITE_VISIT";
    form += "&runSchedule=(start:" + Date.now().toString() + ")";


    var headers2 = {};

    headers2["content-type"] = "application/x-www-form-urlencoded";
    headers2["cookie"] = fs.readFileSync("LD.cnf", "utf-8").split(",")[2];
    headers2["csrf-token"] = fs.readFileSync("LD.cnf", "utf-8").split(",")[1];
    headers2["referer"] =
        "https://www.linkedin.com/campaignmanager/accounts/" +
        accountId +
        "/campaigns/new/details";
    // headers2["user-agent"] = headers["user-agent"];
    headers2["x-http-method-override"] = "GET";
    headers2["x-restli-protocol-version"] = "2.0.0";

    var options = {
        url: "https://www.linkedin.com/campaign-manager-api/campaignManagerLimits",
        headers: headers2,
        body: form,
    };

    requested = requestedComb(dataArray2d);

    let resp = await doRequest(options);
    resp = JSON.parse(resp);
    resp["requested"] = requested;
    unitCostSuggested = resp.elements[0].bidSuggestion.midBid.amount;

    fs.writeFile("suggestion.json", JSON.stringify(resp), function(err) {
        if (err) {
            console.log("Error writing suggestion file: ", err);
        }
    });

    return unitCostSuggested;
}

exports.initSession = initSession;
exports.getBidSuggestion = getBidSuggestion;
exports.accountIdWriter = accountIdWriter;
exports.getURN = getURN;
