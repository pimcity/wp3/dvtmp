const loginForm = document.querySelector('.form--login');

const hideAlert = () => {
  const el = document.querySelector('.alert');
  if (el) el.parentElement.removeChild(el);
};
const showAlert = (type, msg) => {
  hideAlert();
  const markup = `<div class="alert alert--${type}">${msg}</div>`;
  document.querySelector('body').insertAdjacentHTML('afterbegin', markup);
  window.setTimeout(hideAlert, 5000);
};

if (loginForm)
  loginForm.addEventListener('submit', (e) => {
    e.preventDefault();
    const gender = document.getElementById('gender').value;
    const min_age = Number(document.getElementById('min').value);
    const max_age = Number(document.getElementById('max').value);
    const interest = document.getElementById('interest').value.split(',');
    const priceType = document.getElementById('priceType').value;
    const platform = document.getElementById('platform').value;
    const location = document.getElementById('location').value;
    const login = async (
      gender,
      priceType,
      min_age,
      max_age,
      interest,
      platform,
      location
    ) => {
      try {
        const res = await axios({
          method: 'POST',
          url: '/auValuator',
          data: {
            gender,
            min_age,
            max_age,
            interest,
            location,
            platform,
            priceType,
          },
        });
        var txt;
        if (res.status == 201) {
          txt =
            'Realtime data \nEstimated Value :' +
            Math.round(res.data.data.value * 100) / 100 +
            ' $';
        } else if (res.status == 200) {
          txt =
            'We had the data in our database \nEstimated Value :' +
            Math.round(res.data.data.value * 100) / 100 +
            ' $';
        } else {
          txt = ' Please revise the inputs';
        }

        showAlert('success', txt);
      } catch (err) {
        console.log(err);
      }
    };

    login(gender, priceType, min_age, max_age, interest, platform, location);
  });
