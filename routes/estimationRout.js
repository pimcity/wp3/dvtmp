const express = require("express");
const estimationRoutController = require("../controllers/estimationRoutController");
// const keycloak = require('../controllers/authController').getKeycloak();
const router = express.Router();

router
    .route("/")
    // .post(keycloak.protect('user'), estimationRoutController.plaformselector);
    .post(estimationRoutController.plaformselector);

module.exports = router;