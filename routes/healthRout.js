const express = require("express");
// const viewsController = require('../controllers/viewsController');
const healthController = require("../controllers/healthController");
const router = express.Router();

router.get("/", healthController.getHealth);

module.exports = router;
