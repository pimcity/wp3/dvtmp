const express = require('express');
const viewsController = require('../controllers/viewsController');

const router = express.Router();

router.get('/ui', viewsController.getAuForm);
router.get('/', viewsController.getMain);

module.exports = router;
