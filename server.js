const app = require("./app");
const port = process.env.PORT || 3700;
const reports = require("./utils/moduleReports");
app.listen(port, () => {
    console.log(`App running on port ${port}...`);
});
var data = {
    gender: "male",
    interest: ["Engineering"],
    location: "ES",
    max_age: 64,
    min_age: 16,
    platform: "facebook",
    priceType: "CPM",
};
reports(data);