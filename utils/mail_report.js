nodemailer = require("nodemailer");

module.exports = async(e) => {
    try {
        let transporter = nodemailer.createTransport({
            host: "smtp-mail.outlook.com",
            port: 587,
            secure: false,
            auth: {
                user: process.env.PLTEUSR,
                pass: process.env.PLTEPSS,
            },
        });

        let info = await transporter.sendMail({
            from: `"DVTMP @${process.env.NODE_ENV}" dvtmp@outlook.com`,
            to: process.env.ENG,
            subject: "DVTMP  " + " Status " + process.env.NODE_ENV,
            text: e,
            html: "",
        });
        console.log("Message sent: %s", info.messageId);
    } catch (error) {
        console.log(error);
    }
};